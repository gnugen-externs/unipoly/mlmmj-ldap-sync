PREFIX ?= /usr

build:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin /etc /lib/systemd/system
	install -m755 unipoly-mlmmj-ldap-sync $(DESTDIR)$(PREFIX)/bin/unipoly-mlmmj-ldap-sync
	install -m644 conf.example.toml /etc/unipoly-mlmmj-ldap-sync.toml
	install -m644 unipoly-mlmmj-ldap-sync.service unipoly-mlmmj-ldap-sync.timer /lib/systemd/system
